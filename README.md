This is a [tree-sitter](https://tree-sitter.github.io/tree-sitter/) grammar for [TaskJuggler](https://taskjuggler.org/).

# Compile

Just run `make` :)
