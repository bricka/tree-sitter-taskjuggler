(source_file (attribute (attribute_name name: (identifier) @keyword)))

(attribute_name name: (identifier) @attribute)
(attribute_name scenario: (identifier) @variable.parameter)

[(single_quote_string) (double_quote_string)] @string
(multiline_string (multiline_string_start) @punctuation.special (multiline_string_end) @punctuation.special) @string
(number) @number

(relative_identifier "!" @punctuation.special)

(comment) @comment
